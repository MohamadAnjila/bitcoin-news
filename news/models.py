from django.db import models


# Create your models here.
class Article(models.Model):
    title = models.TextField(unique=False,null=True)
    description = models.TextField(unique=False,null=True)
    url = models.TextField(unique=False,null=True)
    urlImage = models.TextField(unique=False, null=True)
    publishedAt = models.DateTimeField()
    content = models.TextField(unique=False,null=True)

    def delete_everything(self):
        Article.objects.all().delete()

    def __str__(self):
        return self.title
