from django.shortcuts import render, get_object_or_404, redirect
from .models import Article
import requests


# Create your views here.
def home(request):
    if request.method == 'POST':
        subject = request.POST['subject']
        if 'search' in request.POST:
            articles = Article.objects.filter(title=subject)
        elif 'date' in request.POST:
            articles = Article.objects.order_by('publishedAt')

    else:
        response = requests.get(
            'http://newsapi.org/v2/everything?q=bitcoin&from=2020-08-21&sortBy=publishedAt&apiKey'
            '=f22316e8e54f4fccadc942e7548c489b')
        geodata = response.json()
        list = geodata['articles']
        Article.objects.all().delete()
        for article in list:
            title = article['title']
            description = article['description']
            url = article['url']
            urlToImage = article['urlToImage']
            publishedAt = article['publishedAt']
            content = article['content']
            article = Article.objects.create(
                title=title,
                description=description,
                url=url,
                urlImage=urlToImage,
                publishedAt=publishedAt,
                content=content
            )
            articles = Article.objects.all()
    return render(request, 'home.html', {'articles': articles})


def description(request, card_id):
    card = get_object_or_404(Article, pk=card_id)

    return render(request, 'description.html', {'card': card})
